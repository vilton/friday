import Vue from 'vue'
import App from './App.vue'
import vueSmoothScroll from 'vue-smoothscroll'
import VueTouch from 'vue-touch'
import VueCarousel from 'vue-carousel'
import { store } from './store/store'
import { router } from './router/router'

VueTouch.registerCustomEvent('doubletap', {
	type: 'tap',
	taps: 3
})
VueTouch.registerCustomEvent('fivetap', {
	type: 'tap',
	taps: 5
})

export const bus = new Vue();

Vue.use(vueSmoothScroll);
Vue.use(VueTouch);
Vue.use(VueCarousel);

new Vue({
	el: '#app',
	router,
	store,
	render: h => h(App)
})
