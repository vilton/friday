import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import PageUnderConstruction from '../components/PageUnderConstruction.vue'
import HomePage from '../components/HomePage.vue'
import PageInterna from '../components/PageInterna.vue'

export const router = new VueRouter({
	routes: [
		{ 
			path: '*',
			redirect: '/'
		},
		{ 
			path: '/wating',
			component: PageUnderConstruction
		},
		{
			path: '/',
			name: 'home',
			component: HomePage
		},
		{
			path: '/p/:projectName',
			component: PageInterna
		},
		{
			path: '/sobre',
			name: 'sobre',
			component: PageInterna
		},
		{
			path: '/ui-ux-design',
			name: 'ui-ux-design',
			component: PageInterna
		},
		{
			path: '/desenvolvimento',
			name: 'desenvolvimento',
			component: PageInterna
		},
		{
			path: '/social-media',
			name: 'social-media',
			component: PageInterna
		},
		{
			path: '/design',
			name: 'design',
			component: PageInterna
		}
	],
});