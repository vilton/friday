import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		portfolioItems: [
			{
				name: 'homni-poa',
				title: 'Homni - Porto Alegre',
				subTitle: 'Gerenciamento da página no Facebook.',
				link: 'https://www.facebook.com/homnipoa/',
				category: 'social-media',
				backgroundImg: '../src/assets/portfolio/social-media/homni-poa.jpg',
				thumbnail: '../src/assets/portfolio/social-media/homni-thumb.jpg',
				icon: '../src/assets/clients/homni.png',
				color: 'rgba(27, 55, 121, .92)'
			},
			{
				name: 'teixeira-ribeiro',
				title: 'Teixeira Ribeiro',
				subTitle: 'Design e desenvolvimento do Site.',
				link: 'https://teixeiraribeiro.com',
				category: 'Design, Desenvolvimento',
				backgroundImg: '../src/assets/portfolio/teixeira-ribeiro.jpg',
				thumbnail: '../src/assets/portfolio/teixeira-thumb.jpg',
				icon: '../src/assets/portfolio/icon-teixeira.png',
				color: 'rgba(31, 34, 41, .92)'
			},
			{
				name: 'um-novo-olhar',
				title: 'Um Novo Olhar',
				subTitle: 'Design e desenvolvimento do Site.',
				link: 'http://smart.arq.br/um-novo-olhar/',
				category: 'Design, Desenvolvimento',
				backgroundImg: '../src/assets/portfolio/umnovoolhar.jpg',
				thumbnail: '../src/assets/portfolio/umnovoolhar-thumb.jpg',
				icon: '../src/assets/portfolio/icon-umnovoolhar.png',
				color: 'rgba(0, 0, 0, .92)'
			},
			{
				name: 'senai',
				title: 'Senai',
				subTitle: 'Desenvolvimento de Site.',
				link: 'http://institutossenai.org.br/',
				category: 'desenvolvimento',
				backgroundImg: '../src/assets/portfolio/sites/senai.jpg',
				thumbnail: '../src/assets/portfolio/sites/senai-thumb.jpg',
				icon: '../src/assets/clients/senai.png',
				color: 'rgba(28, 157, 210, .92)'
			},
			{
				name: 'smart',
				title: 'Smart',
				subTitle: 'Desenvolvimento de Site.',
				link: 'http://smart.arq.br/',
				category: 'desenvolvimento',
				backgroundImg: '../src/assets/portfolio/sites/smart.jpg',
				thumbnail: '../src/assets/portfolio/sites/smart-thumb.jpg',
				icon: '../src/assets/clients/smart.png',
				color: 'rgba(0, 0, 0, .92)'
			},
			{
				name: 'via-corpo',
				title: 'Academia Via Corpo',
				subTitle: 'Gerenciamento da página no Facebook.',
				link: 'https://www.facebook.com/academiaviacorpofitness/',
				category: 'social-media',
				backgroundImg: '../src/assets/portfolio/social-media/viacorpo.jpg',
				thumbnail: '../src/assets/portfolio/social-media/viacorpo-thumb.jpg',
				icon: '../src/assets/clients/viacorpo.png',
				color: 'rgba(139, 206, 44, .92)'
			},
			{
				name: 'pr-hansen',
				title: 'PR Hansen - Manutenção',
				subTitle: 'Identidade Visual',
				link: 'https://www.facebook.com/academiaviacorpofitness/',
				category: 'design',
				backgroundImg: '../src/assets/portfolio/branding/pr-hansen.jpg',
				thumbnail: '../src/assets/portfolio/branding/pr-hansen-thumb.jpg',
				icon: '../src/assets/clients/pr-hansen.png',
				color: 'rgba(252, 219, 80, .92)'
			},
			{
				name: 'deluxe',
				title: 'Deluxe Midia',
				subTitle: 'Identidade Visual',
				link: 'https://www.somosdeluxe;com.br',
				category: 'design',
				backgroundImg: '../src/assets/portfolio/branding/deluxe.jpg',
				thumbnail: '../src/assets/portfolio/branding/deluxe-thumb.jpg',
				icon: '../src/assets/clients/deluxe.png',
				color: 'rgba(254, 181, 17, .92)'
			},
			{
				name: 'alquimia-da-cerveja',
				title: 'Alquimia da Cerveja',
				subTitle: 'Gerenciamento da página no Facebook.',
				link: 'https://www.facebook.com/alquimiadacerveja/',
				category: 'social-media',
				backgroundImg: '../src/assets/portfolio/social-media/alquimia-da-cerveja.jpg',
				thumbnail: '../src/assets/portfolio/social-media/alquimia-thumb.jpg',
				icon: '../src/assets/clients/alquimia-da-cerveja.png',
				color: 'rgba(52, 32, 20, .92)'
			},
			{
				name: 'nuvem',
				title: 'Nuvem',
				subTitle: 'Desenvolvimento de Site.',
				link: 'http://nuvem.cc/',
				category: 'desenvolvimento',
				backgroundImg: '../src/assets/portfolio/sites/nuvem.jpg',
				thumbnail: '../src/assets/portfolio/sites/nuvem-thumb.jpg',
				icon: '../src/assets/clients/nuvem.png',
				color: 'rgba(62, 190, 190, .92)'
			},
			{
				name: 'via-corpo-branding',
				title: 'Academia Via Corpo - Identidade Visual',
				subTitle: 'Identidade Visual',
				link: 'https://www.facebook.com/academiaviacorpofitness/',
				category: 'design',
				backgroundImg: '../src/assets/portfolio/branding/viacorpo-branding.jpg',
				thumbnail: '../src/assets/portfolio/branding/viacorpo-branding-thumb.jpg',
				icon: '../src/assets/clients/viacorpo.png',
				color: 'rgba(139, 206, 44, .92)'
			}
		],
		paginaInternaObj: {
			sobre: {
				title: 'Sobre nós',
				pageTitle: 'sobre',
				background: '../src/assets/what-we-do/team.jpg',
				subTitle: 'Somos um parceiro confiável, inventivo e focados em resultados.',
				infoText: 'Trabalhamos com criatividade, paixão pelo que somos e pelo que fazemos, buscando proporcionar as melhores experiências na apresentação das marcas de nossos clientes.',
				perks: [
					{
						title: 'Equipe qualificada',
						text: 'Experiência, trabalho duro, foco!'
					},
					{
						title: 'Suporte individual',
						text: 'Estamos disponíveis para nosso clientes a qualquer momento que for necessário.'
					},
					{
						title: 'Desenvolvimento de sites',
						text: 'Criamos o site certo para sua empresa.'
					},
					{
						title: 'Design moderno',
						text: 'Nossa equipe está sempre atenta as tendências.'
					},
					{
						title: 'Sites rápidos',
						text: 'Você precisa de um site pra ontem, podemos ajuda-lo!'
					},
					{
						title: 'Branding',
						text: 'Criamos a marca perfeita para sua ideia.'
					},
				],
			},
			desenvolvimento: {
				title: 'Desenvolvimento Web',
				pageTitle: 'desenvolvimento',
				background: '../src/assets/what-we-do/dev.jpg',
				subTitle: 'Sites atrativos e com tecnologia de ponta, que trazem sua empresa para o universo digital.',
				infoText: 'Desenvolvemos sites focados na conversão de usuários em clientes. Projetos customizados de ponta-a-ponta para o seu negócio. Após conhecer sua marca, criaremos a melhor solução em design e tecnologia para alcançar o seu público',
				perks: [
					{
						title: 'Web Design',
						text: 'Criamos interfaces com design responsivo, de fácil acesso e atrativas por meio de técnicas que aumentam a usabilidade da página na web.'
					},
					{
						title: 'Sites Responsivos',
						text: 'Todos os nossos sites se adaptam ao aparelho onde seu cliente está acessando. Celulares, tablets, computadores, Tvs.'
					},
					{
						title: 'Gerenciador de Conteúdo',
						text: 'Você terá uma interface simples e intuítiva para poder mudar o conteúdo do seu site. Sem precisar contatar alguém sempre que quiser mudar uma palavra ou imagem.'
					},
					{
						title: 'E-Commerce',
						text: 'Venda seu produto online, para qualquer lugar e a qualquer hora.'
					},
					{
						title: 'Sites rápidos',
						text: 'Você precisa de um site pra ontem, podemos ajuda-lo!'
					},
					{
						title: 'Suporte',
						text: 'Nossa equipe está disponível para prestar suporte, resolver qualquer dúvida ou fazer ajustes no seu site.'
					},
				],
			},
			socialMedia: {
				title: 'Social Media',
				pageTitle: 'social-media',
				background: '../src/assets/what-we-do/social-media.jpg',
				subTitle: 'Promovemos o seu negócio na internet para conectá-lo ao seu público, gerando resultados positivos.',
				infoText: 'Analisamos onde seu público está, qual a melhor estratégia para cada rede social, que conteúdo produzir, como fazer a criação de anúncios e campanhas, além de monitorar e mensurar os resultados.',
				perks: [
					{
						title: 'Criação',
						text: 'Criamos ou atualizamos o perfil da sua empresa nas Redes Sociais.'
					},
					{
						title: 'Comunicação Interativa',
						text: 'Ao contrário do marketing tradicional, o digital é uma via de mão dupla, ou seja, um canal aberto de comunicação entre as marcas e seu consumidor.'
					},
					{
						title: 'Postagem',
						text: 'Postamos regularmente durante a semana, conforme a estratégia criada.'
					},
					{
						title: 'Linguagem',
						text: 'Seguimos sempre a mesma linguagem e identidade visual.'
					},
					{
						title: 'Análise',
						text: 'Analisamos as métricas e resultados alcançados para melhorar ainda mais.'
					},
					{
						title: 'Custo-benefício',
						text: 'A combinação de todas essas características torna o custo-benefício do marketing digital muito vantajoso.'
					}
				],
			},
			design: {
				title: 'Design',
				pageTitle: 'design',
				background: '../src/assets/what-we-do/visual.jpg',
				subTitle: 'Estudamos conceitos de marca para criar uma comunicação visual única',
				infoText: 'Nosso trabalho é alinhado com as necessidades da sua empresa, com foco em estabelecer vínculos entre o seu público e a sua marca.',
				perks: [
					{
						title: 'Identidade Visual',
						text: 'Analisamos ideias e conceitos para desenvolver uma identidade visual única que representa a essência da sua marca, agrega valor e a diferencia no mercado.'
					},
					{
						title: 'Web Design',
						text: 'Criamos interfaces com design responsivo, de fácil acesso e atrativas por meio de técnicas que aumentam a usabilidade da página na web.'
					},
					{
						title: 'Materiais Gráficos',
						text: 'O material gráfico é importante para transmitir informações sobre sua empresa. Estudamos conceitos gráficos e da sua marca para entregar o melhor resultado.'
					},
					{
						title: 'Mídia Online e Offline',
						text: 'Trabalhamos com o planejamento e criação de campanhas online e offline unindo informações que integram todos os pontos da empresa, produto e serviço.'
					}
				],
			}
		},
	}
});